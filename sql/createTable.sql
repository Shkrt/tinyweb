CREATE TABLE measurements
(
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	rec_time DATETIME DEFAULT CURRENT_TIMESTAMP,
	out_temp REAL,
	out_hum REAL,
	in_temp REAL,
	in_hum REAL,
	athm_p REAL	
);
