import serial
import re
import TinyMeteo
import storage
from datetime import datetime, date
import sys

if len(sys.argv)!=2:
	print "no config file was supplied, exiting..."	
	sys.exit()

config={}
execfile(sys.argv[1],config)
port=serial.Serial(config["port"])
dbfile=config["datasource"]

def read_val(com):
	com.write("$rdval\r\n")
	rawdata=com.readline()
	com.close()
	return rawdata

def parse_raw(data):
	lst=re.findall("\d*[.]\d*|\d*",data)
	return filter(lambda x: x!='',lst)	

values=parse_raw(read_val(port))
if len(values)==5:
	tm=TinyMeteo.TinyMeteo(
		outer_temp=values[0],
		outer_humid=values[1],
		inner_temp=values[2],
		inner_humid=values[3],
		athm_pressure=values[4])
	st=storage.Sqlite(dbfile)
	st.store(tm) 	

else:
	print "There was no data received"


