import sqlite3

class StorageProvider(object):
	def __init__(self,dir):
		self.datadir=dir		

	def store(self,data):
		raise NotImplementedError("This is the abstract method")

	def show(self,count):
		raise NotImplementedError("This is the abstract method")

class Sqlite(StorageProvider):
	def store(self,data):
		conn=sqlite3.connect(self.datadir)
		c=conn.cursor()
		query=("""insert into measurements (out_temp,out_hum,in_temp,in_hum,athm_p) 
				VALUES ({0},{1},{2},{3},{4})""".format(					
					data.outer_temp,
					data.outer_humid,
					data.inner_temp,
					data.inner_humid,
					data.athm_pressure))
		c.execute(query)
		conn.commit()
		conn.close()
	

	def show(self,count):
		lst=[]
		conn=sqlite3.connect(self.datadir)
		c=conn.cursor()
		query="""select * from measurements limit {0}""".format(count)
		for row in c.execute(query):
			lst.append(row)	
		conn.close()
		print (lst)

			
