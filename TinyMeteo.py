class TinyMeteo(object):
	
	def __init__(self,				
		outer_temp=float('NaN'),
		outer_humid=float('NaN'), 
		inner_temp=float('NaN'),
		inner_humid=float('NaN'),
		athm_pressure=float('NaN')):
		
		self.inner_humid=inner_humid
		self.outer_humid=outer_humid
		self.inner_temp=inner_temp
		self.outer_temp=outer_temp
		self.athm_pressure=athm_pressure

	def __str__ (self):
		lst=[]		
		for property, value in vars(self).iteritems():
			next= ':'.join([repr(property),repr(value)])
			lst.append(next)
		return "\r\n".join(lst)

